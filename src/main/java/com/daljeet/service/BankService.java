package com.daljeet.service;

import com.daljeet.model.Bank;

public class BankService {
    private Bank sbiBank;
    private Bank iciciBank;

    public Bank getSbiBank() {
        return sbiBank;
    }

    public void setSbiBank(Bank sbiBank) {
        this.sbiBank = sbiBank;
    }

    public Bank getIciciBank() {
        return iciciBank;
    }

    public void setIciciBank(Bank iciciBank) {
        this.iciciBank = iciciBank;
    }

    public Bank getHighestInterestBank() {
        if (sbiBank.getInterestRate() > iciciBank.getInterestRate()) {
            return sbiBank;
        } else {
            return iciciBank;
        }
    }
}