package com.daljeet.model;

public interface Bank {
    double getInterestRate();
    void setInterestRate(double interestRate);
    String getName();
    void setName(String name);
}