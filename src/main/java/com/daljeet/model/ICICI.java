package com.daljeet.model;

public class ICICI implements Bank {
    private double interestRate;
    private String name;

    public ICICI() {
        this.name = "ICICI Bank";
        this.interestRate = 8.0;
    }

    public double getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(double interestRate) {
        this.interestRate = interestRate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}