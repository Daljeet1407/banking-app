package com.daljeet.model;

public class SBI implements Bank {
    private double interestRate;
    private String name;

    public SBI() {
        this.name = "State Bank of India";
        this.interestRate = 8.5;
    }

    public double getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(double interestRate) {
        this.interestRate = interestRate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}