package com.daljeet;

import com.daljeet.model.Bank;
import com.daljeet.service.BankService;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;

public class BankTest {

    public static void main(String[] args) {
        // Using BeanFactory
        BeanFactory beanFactory = new XmlBeanFactory(new ClassPathResource("bank-config.xml"));
        BankService bankService = beanFactory.getBean(BankService.class);
        Bank highestInterestBank = bankService.getHighestInterestBank();
        System.out.println("Highest Interest Bank (BeanFactory): " + highestInterestBank.getName() + " with interest rate " + highestInterestBank.getInterestRate());

        // Using ApplicationContext
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("bank-config.xml");
        BankService bankService2 = applicationContext.getBean(BankService.class);
        Bank highestInterestBank2 = bankService2.getHighestInterestBank();
        System.out.println("Highest Interest Bank (ApplicationContext): " + highestInterestBank2.getName() + " with interest rate " + highestInterestBank2.getInterestRate());
    }

}